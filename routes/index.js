var app = require('../app');
var Post = require('../models/post').Post;

app.get('/api/news', function (req, res) {
    // get the max distance or set it to 8 kilometers
    var maxDistance = req.query.radius || 8;

    // we need to convert the distance to radians
    // the raduis of Earth is approximately 6371 kilometers
    maxDistance /= 6371;

    // get coordinates [ <longitude> , <latitude> ]
    var coords = [];
    coords[0] = req.query.longitude;
    coords[1] = req.query.latitude;

    // find a location
    Post.find({
        location: {
            $near: coords,
            $maxDistance: maxDistance
        }
    }).exec(function(err, locations) {
        if (err) {
            return res.json(500, err);
        }

        res.json(200, locations);
    });
});

app.get('/api/words', function(req, res) {

    var map = function() {
       var summary = this.content;
       if (summary) {
           var words = summary.toLowerCase().split(' ');

           words.forEach(function(word) {
               word = word.trim();
               if (word.length && isNaN(word)) {
                   emit(word, 1);
               }
           })
       }
    };

    var reduce = function (key, values) {
        var count = 0;

        values.forEach(function(v) {
            count += v;
        });

        return count;
    };

    Post.mapReduce({
        map: map,
        reduce: reduce,
        sort: {
            value: -1
        }
    }, function(err, results, stats) {
        results.sort(function(current, next) {
            return next.value - current.value;
        });

        var responseData = {};

        results.forEach(function(item) {
            responseData[item._id] = item.value;
        });

        res.json(200, responseData);
    });
});