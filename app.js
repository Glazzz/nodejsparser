const express = require('express');
const mongoose = require('mongoose');
const config = require('config');

var app = module.exports = express();
var port = config.get('port');

function connect () {
    mongoose.Promise = require('bluebird');
    return mongoose
        .connect(config.get('mongoose.uri'), config.get('mongoose.options'))
        .connection;
}

function listen () {
    app.listen(port);
    console.log('Express app started on port ' + port);
}

require('./routes');
require('./console');

connect()
    .on('error', console.log)
    .on('disconnected', connect)
    .once('open', listen);
