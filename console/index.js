const config = require('config');
const osmosis = require('osmosis');
const async = require('async');
const Post = require('../models/post').Post;

var argv = require('optimist')
    .usage('Parsing posts from news.tut.by')
    .option('s', {
        alias: 'show'
    })
    .option('p', {
        alias: 'parsing'
    })
    .option('c', {
        alias: 'count',
        default: config.get('maxParsingItemsSize')
    })
    .argv;

if (argv.parsing) {
    runNewsParsing();
}

if (argv.show) {
    showRecords();
}

function showRecords() {
    Post.find({}, function(err, results) {
        console.log(results);
    });
}

function runNewsParsing() {
    var maxParsingItemsSize = argv.c;
    var parsedData = [];

    osmosis
        .get(config.get('parsingUrl'))
        .find('.l-columns')
        .follow('.news-entry > a[href]:limit(' + maxParsingItemsSize + ')')
        .set({
            'url': 'meta[property="og:url"]@content',
            'lat': 'meta[itemprop="latitude"]@content',
            'lng': 'meta[itemprop="longitude"]@content'
        })
        .find('.b-article')
        .set({
            'title': 'h1',
            'datePublished': 'time[itemprop="datePublished"]@datetime',
            'content': '#article_body'
        })
        .data(function (data) {
            data.datePublished = new Date(data.datePublished);
            data.sourceId = Post.schema.parseSourceIdByUrl(data.url);


            parsedData.push(data);
        })
        .error(console.log)
        .done(function () {
            console.log('Parsing has been done');

            async.each(parsedData, function (postData, callback) {
                var lat = typeof postData.lat !== 'undefined' ? postData.lat : 0;
                var lng = typeof postData.lat !== 'undefined' ? postData.lng : 0;
                Post.createOrUpdate({
                    'sourceId': postData.sourceId
                }, {
                    sourceId: postData.sourceId,
                    title: postData.title,
                    content: postData.content,
                    datePublished: postData.datePublished,
                    location: [lat, lng]
                }).then(function (data) {
                    console.log('Record with sourceId = ' + data.sourceId + ' has been saved');
                }).catch(function(err) {
                    console.log('Error ['+ err.code +']: ' + err.message);
                });
            }, console.log);
        });
}
