const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Turf = require('turf');

var schema = new Schema({
    sourceId: {
        type: Number,
        unique: true,
        required: true
    },
    datePublished: {
        type: Date,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    content: {
        type: String,
        required: false
    },
    location: {
        type: [Number],  // [<longitude>, <latitude>]
        index: '2d'      // create the geospatial index
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
});

schema.plugin(require('mongoose-create-or-update'));

schema.methods.searchByCoordinates = function (lat, lng, radius) {
    var point = Turf.point([lat, lng]);
    var resultsData = [];
    mongoose.model('Post', schema).find({}, function(err, results) {
        results.forEach(function(item) {
            console.log(item.lat, item.lng);
            if (Turf.distance(point, [item.lat, item.lng]) <= radius) {
                resultsData.push(item);
            }
        })
    });

    return resultsData;
};

schema.parseSourceIdByUrl = function(url) {
    // Save only post id number
    this.sourceId = url.split('/').pop().match(/\d/g).join('');

    return this.sourceId;
};

exports.Post = mongoose.model('Post', schema);